# Mekamon Smart Module: mekamon\_remote ROS package.


ROS package demo client nodes for controlling the mekamon\_base base\_controller node.

##mekamon\_ros\_remote package
The mekamon\_ros\_remote package has an example node for controlling the base\_controller, written in Python 2.7:

**demo\_client\_node.py** in the scripts folder. 

This uses an appJar (TKinter) GUI and so depends on the module appJar.
    
The comments in the .py file outline how to format the various commands for base_controller.

Also included is a demo joystick control node:

**teleop_joy.cpp** in the 'src' folder. 

This depends on the ROS package 'Joy'.
