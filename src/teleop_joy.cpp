 /* mm_teleop_joy_node.cpp
  * Wes Freeman 2018
  * 
  * Converts joystick driver values into ROS geometry_msgs/Twist messages.
  * 
  * Based on http://wiki.ros.org/joy/Tutorials/WritingTeleopNode,
  * adapted for holonomic Mekamon robot.
  * 
  * Requires the Joy ROS package from http://wiki.ros.org/joy, ros-kinetic-joy
*/

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>

/** Converts joystick driver values into ROS Twist messages.
 */
class TeleopMM
{
    public:
      TeleopMM();

    private:
      void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

      ros::NodeHandle nh_;

      int linearX_, linearY_, angular_;
      double l_scale_, a_scale_;
      ros::Publisher vel_pub_;
      ros::Subscriber joy_sub_;
};

/** Constructor. Defines which axes of the joystick will be used,
 * creates ROS publisher & subscriber.
 */
TeleopMM::TeleopMM():
  linearX_(0),
  linearY_(1),
  angular_(2),
  a_scale_(1.0), 
  l_scale_(1.0)
{

  //check the parameter server for new scalar values for driving the turtle:
  // (from launch file)
  nh_.param("axis_linearX", linearX_, linearX_);
  nh_.param("axis_linearY", linearY_, linearY_);
  nh_.param("axis_angular", angular_, angular_);
  nh_.param("scale_angular", a_scale_, a_scale_);
  nh_.param("scale_linear", l_scale_, l_scale_);

  vel_pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);

  joy_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &TeleopMM::joyCallback, this);
  // up to 10 messages will be buffered before any are lost.
}

/** Callback triggered by ROS subscription to the Joy topic.
 * Creates a ROS geometry_msgs/Twist velocities container.
 * Converts the Joy values and writes them into the msg.
 * Publishes the message.
 * 
 * @param joy
 */
void TeleopMM::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
  geometry_msgs::Twist twist;
  twist.angular.z = -1*a_scale_*joy->axes[angular_]; // reversed
  twist.linear.x = -1*l_scale_*joy->axes[linearX_]; // reversed
  twist.linear.y = l_scale_*joy->axes[linearY_]; 
  vel_pub_.publish(twist);
}

/** main() inits ROS node, spawns TeleopMM object, checks for ROS callbacks.
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv)
{
  ros::init(argc, argv, "mm_teleop_joy_node");
  TeleopMM teleop_mm;

  ros::spin();
}